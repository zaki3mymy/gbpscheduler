/*
* @param {string} calendarCell スケジュールのdiv要素
* @param {string} yyyymm
* @return {object} 日付とイベントのオブジェクト
*   info = {
*     "date": Date,
*     "content": [
*       {"title": "title1", color: EventColor},
*       {"title": "title2", color: EventColor},
*       ...
*     ]
*   }
*/
function extractEventInfo(calendarCell, ym){
  // calendarCellのHTML構造は以下のような感じ。
  // <div class="calendarCell">
  //   <div class="calendarCellDate">dd<span>weekday</span></div>
  //   <div class="calendarCellContent">
  //     <a class="... color ...">title1</a>
  //     <a class="... color ...">title2</a>
  //     ...
  //   </div>
  // </div>
  
  let calendarCellDate = parser.getElementsByClassName(calendarCell, "calendarCellDate");
  let dt = null;
  if (calendarCellDate.length != 0){
    // 日付の整形
    let day = ("0" + calendarCellDate[0].getText()).slice(-2);
    let year = ym.substring(0, 4);
    let month = Number(ym.substring(4, 6))-1;
    dt = new Date(year, month, day);
  } else {
    // 日付がない場合はnull
    return null;
  }

  // カレンダーに登録する色
  // イベント情報の色は色コードでの指定はできない。
  let colorDict = {
    "default": CalendarApp.EventColor.PALE_GREEN,
    "anime": CalendarApp.EventColor.PALE_RED,
    "goods": CalendarApp.EventColor.CYAN,
    "event": CalendarApp.EventColor.BLUE,
    "media": CalendarApp.EventColor.ORANGE,
    "release": CalendarApp.EventColor.YELLOW
  }

  let contents = [];
  let calendarCellContent = parser.getElementsByClassName(calendarCell, "calendarCellContent");  
  if (calendarCellContent) {
    // タイトルと色を取得
    contents = parser.getElementsByTagName(calendarCellContent[0], "a")
      .map(ele => {
        // 色の取得
        let cls = ele.getAttribute("class").getValue();
        let color = colorDict["default"];
        Object.keys(colorDict).forEach(key =>{
          if (cls.indexOf(key) > 0){
            color = colorDict[key];
          }
        })
        // タイトルと色の組を取得
        return {
          "title": ele.getValue(),
          "color": color
        }
      })
  }

  let info = {
    "date": dt,
    "contents": contents
  }
  return info;
}

function formatHtml(res){
  // body 抽出
  res = res.match(/<body(.|\r|\n)*<\/body>/)[0];
  // imgタグ削除。。。
  res = res.replace(/<img .*?>/g, "");
  // scriptタグ削除。。。
  res = res.replace(/<script.*>.*<\/script>/g, "");
  // brタグ削除。。。
  res = res.replace(/<br.*?>/g, "");
  
  return res;
}

/**
* @param {integer} addMonth 当月から何ヶ月±するか
* @return {string} yyyymm 形式の文字列
*/
function getYearMonth(addMonth=0){
  let dt = new Date();
  dt.setMonth(dt.getMonth() + addMonth);
  // getMonth()は0-11なので+1
  let ym = dt.getFullYear() + ("0" + (dt.getMonth() + 1)).slice(-2);
  return ym;
}

/**
* @param {number} addMonth 当月から何ヶ月先のスケジュールを取得するか
* @return {Array.<object>} イベント情報リスト
*/
function scraping(addMonth=0){
  // バンドリ公式サイトからSchedule情報を取得する
  
  let ym = getYearMonth(addMonth)
  const url = "https://bang-dream.com/news/schedule?ym=" + ym;

  let res = UrlFetchApp.fetch(url).getContentText();
  res = formatHtml(res);

  let doc = XmlService.parse(res);
  let html = doc.getRootElement();
  let calendarRows = parser.getElementsByClassName(html, "calendarRow");
  
  // イベント情報リストを抽出する
  let eventInfoList = [];
  calendarRows.forEach(row => {
    //row.getChildren("div").forEach(calendarCell => {
    parser.getElementsByClassName(row, "calendarCell").forEach(calendarCell => {
      let cls = calendarCell.getAttribute("class").getValue();
      if (cls.indexOf("notCurrent") < 0){
        // 当月のcellのclassには"notCurrent"がない
        let info = extractEventInfo(calendarCell, ym);
        if (info) eventInfoList.push(info);
      }
    })
  });
  Logger.info(eventInfoList);
  return eventInfoList;
}
