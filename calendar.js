/**
* カレンダーにイベントを登録する。
* @param {CalendarEvent} calendar イベントを登録するカレンダー
* @param {Array.<object>} eventInfoList イベント情報
*/
function addEventToCalendar(calendar, eventInfoList) {
  eventInfoList.forEach(eventInfo => {
    let dt = eventInfo["date"];
    eventInfo["contents"].forEach(content => {
      let title = content["title"];
      let color = content["color"];
      let event = calendar.createEvent(title, dt, dt);
      event.setAllDayDate(dt);
      event.setColor(color);
    })
    Utilities.sleep(1000);
  })
}

function test_addEventToCalendar(){
  let calendars = CalendarApp.getCalendarsByName("GBP");
  let eventInfoList = [
    {"date": new Date(), "contents":[
      {"title": "a", "color": CalendarApp.EventColor.PALE_GREEN},
      {"title": "b", "color": CalendarApp.EventColor.BLUE}
    ]}
  ]

  addEventToCalendar(calendars[0], eventInfoList);
}
