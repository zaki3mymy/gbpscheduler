const CALENDAR_NAME = "GBP";

/**
* カレンダーを作成する。
*/
function initCalendar(){
  // カレンダー削除
  let calendars = CalendarApp.getCalendarsByName(CALENDAR_NAME);
  calendars.forEach(cal => {
    cal.deleteCalendar()
  });

  // カレンダー作成
  let calendar = CalendarApp.createCalendar(CALENDAR_NAME);
  calendar.setColor("#e4004f");
}

/**
* カレンダーに登録されているイベントを削除する。
*/
function deleteAllEvent(){
  let calendars = CalendarApp.getCalendarsByName(CALENDAR_NAME);
  let calendar = calendars[0];
  deleteAllEvent(calendar);
  let startDate = new Date(2018, 0, 1);
  let endDate = new Date(2022, 11,31);
  calendar.getEvents(startDate, endDate).forEach(event => {
    event.deleteEvent()
    Utilities.sleep(500);
  })
}

/**
* 公式サイトからイベント情報を取得してカレンダーに登録する。
*/
function main() {
  Logger.log("start");
  let calendars = CalendarApp.getCalendarsByName(CALENDAR_NAME);
  let calendar = calendars[0];

  let start = 0
  for(let i=start; i<start+3; i++){
    // 公式サイトから月ごとのイベント情報を取得
    eventInfoList = scraping(i);
    // カレンダーに登録
    addEventToCalendar(calendar, eventInfoList);
  }
  Logger.log("end.");
}
