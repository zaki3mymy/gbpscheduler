# Overview
バンドリ公式サイトのスケジュールをGoogleカレンダーに追加するGASスクリプト。

# Usage
clasp等でGASにアップロードし、`main.js`の`initCalendar`を実行してから`main`を実行すると`CALENDAR_NAME`で指定した名前のカレンダーが追加される。
また、`deleteAllEvent`を実行するとカレンダーに登録されているイベントを(雰囲気)全て削除する。